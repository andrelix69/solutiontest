USE [test_base]
go
create table dbo.[system_types]
(
	id int not null primary key identity,
	[name] varchar(50) not null
);

go

insert into  dbo.[system_types] ([name]) 
values ('zomato'),('uber'),('talabat');

go

create table dbo.[orders]
(
	id int not null primary key identity,
	system_type int not null foreign key references  dbo.[system_types](id),
	order_number varchar(20) not null,
	source_order varchar(max) not null,
	converted_order varchar(max) null,
	created_at datetime not null default GETDATE()
);