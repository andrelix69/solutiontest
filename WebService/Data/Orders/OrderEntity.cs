﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebService.Data.SystemTypes;

namespace WebService.Data.Orders
{
    public class OrderEntity:Entity
    {
        [Column("system_type")]
        public int SystemType { get; set; }

        [Column("order_number")]
        public string OrderNumber { get; set; }

        [Column("source_order")]
        public string SourceOrder { get; set; }

        [Column("converted_order")]
        public string ConvertedOrder { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
    }
}
