﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.Orders
{
    public interface IOrderRepository
    {
        OrderEntity[] GetOrderEntities();
        OrderEntity[] GetNewOrderEntities();
        Task SaveAsync(OrderEntity orderEntity);

        Task UpdateAsync(OrderEntity orderEntity);
    }
}
