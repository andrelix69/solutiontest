﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.Orders
{
    public class OrderRequest
    {
        public string OrderNumber { get; set; }
        public Products.Product[] Products { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
