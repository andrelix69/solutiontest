﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.Orders
{
    public class OrderRepository : RepositoryBase, IOrderRepository //некрасиво но для примера сойдёт 
    {
        public OrderRepository(CustomerContext dbContext) : base(dbContext)
        {
        }

        public OrderEntity[] GetOrderEntities()
        {
           return _dbContext.Orders.ToArray();
        }

        public OrderEntity[] GetNewOrderEntities()
        {
            return _dbContext.Orders.Where(w=>w.ConvertedOrder==null).ToArray();
        }

        public async Task SaveAsync(OrderEntity orderEntity)
        {
            _dbContext.Orders.Add(orderEntity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(OrderEntity orderEntity)
        {
            _dbContext.Orders.Update(orderEntity);
            await _dbContext.SaveChangesAsync();
        }
    }
}
