﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using WebService.Data.Orders;

namespace WebService.Data
{
    public class CustomerContext : DbContext
    {
        public CustomerContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<SystemTypes.SystemTypesEntity> SystemTypes => Set<SystemTypes.SystemTypesEntity>();
        public DbSet<OrderEntity> Orders => Set<OrderEntity>();
    }
}
