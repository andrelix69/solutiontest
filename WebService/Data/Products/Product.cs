﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.Products
{
    public class Product
	{        
		public string Id { get; set; }
		public string Name { get; set; }
		public string Comment { get; set; }
		public int Quantity { get; set; }
		public decimal PaidPrice { get; set; }
		public decimal UnitPrice { get; set; }
		public string RemoteCode { get; set; }
		public string Description { get; set; }
		public int? VatPercentage { get; set; }
		public decimal? DiscountAmount { get; set; }
	}
}
