﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.SystemTypes
{
    [Table("system_types")]
    public class SystemTypesEntity:Entity
    {
        public string Name { get; set; }
    }
}
