﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.SystemTypes
{
    public class SystemTypesRepository : RepositoryBase, ISystemTypesRepository
    {
        public SystemTypesRepository(CustomerContext dbContext) : base(dbContext)
        {
        }

        public SystemTypesEntity[] GetSystemTypes()
        {
           return _dbContext.SystemTypes.ToArray();
        }
    }
}
