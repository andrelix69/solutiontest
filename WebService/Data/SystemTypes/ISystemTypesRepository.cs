﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data.SystemTypes
{
    public interface ISystemTypesRepository
    {
        SystemTypesEntity[] GetSystemTypes();
    }
}
