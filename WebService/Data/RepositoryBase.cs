﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Data
{
    public class RepositoryBase
    {
        internal CustomerContext _dbContext;
        public RepositoryBase(CustomerContext dbContext)
        {
            _dbContext = dbContext;
        }

    }
}
