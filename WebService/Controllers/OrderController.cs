﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebService.Data.Orders;

namespace WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        Data.SystemTypes.ISystemTypesRepository _systemTypesRepository;
        IOrderRepository _orderRepository;
        public OrderController(ILogger<OrderController> logger, Data.SystemTypes.ISystemTypesRepository systemTypesRepository,IOrderRepository orderRepository)
        {
            _logger = logger;
            _systemTypesRepository = systemTypesRepository;
            _orderRepository = orderRepository;
        }

        [HttpPost("/{type}")]
        public async Task<IActionResult> Post([FromBody] OrderRequest orderRequest,string type)
        {
            var systemTypes = _systemTypesRepository.GetSystemTypes();

            var systemType = systemTypes.SingleOrDefault(a => a.Name == type);

            if (systemType==null)
            {
                return BadRequest("Type not found");
            }

            OrderEntity orderEntity = new OrderEntity();
            orderEntity.SourceOrder = Newtonsoft.Json.JsonConvert.SerializeObject(orderRequest);
            orderEntity.OrderNumber = orderRequest.OrderNumber;
            orderEntity.CreatedAt = orderRequest.CreatedAt.GetValueOrDefault();
            orderEntity.SystemType = systemType.Id;
            await _orderRepository.SaveAsync(orderEntity);
            return Ok();
        }
    }
}
