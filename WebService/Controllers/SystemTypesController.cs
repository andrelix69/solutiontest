﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SystemTypesController : ControllerBase
    {
        private readonly ILogger<SystemTypesController> _logger;
        Data.SystemTypes.ISystemTypesRepository _systemTypesRepository;
        public SystemTypesController(ILogger<SystemTypesController> logger,Data.SystemTypes.ISystemTypesRepository systemTypesRepository)
        {
            _logger = logger;
            _systemTypesRepository = systemTypesRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_systemTypesRepository.GetSystemTypes());
        }
    }
}
