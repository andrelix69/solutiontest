﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebService.Data.Products;

namespace WebService.Handler
{
    [Handler("talabat")]
    public class TalabatHandler : IHandler
    {
        public void Hand(Product[] products)
        {
            foreach(var item in products)
            {
                item.PaidPrice = -(item.PaidPrice);
            }
        }
    }
}
