﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebService.Handler
{
    public class HandlerFactory
    {
        public static IHandler GetHandler(string type)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();// по хорошему IHandler - нужно поместить в оддельную Dll + обработчики так же в отдельных dll
            var types= assembly.GetTypes().Where(w => w.CustomAttributes.Any(a => a.AttributeType == typeof(HandlerAttribute))).ToArray();
            var typeresult = types.SingleOrDefault(s => (s.GetCustomAttribute(typeof(HandlerAttribute)) as HandlerAttribute)?.Type == type);
            return Activator.CreateInstance(typeresult) as IHandler;
        }
    }
}
