﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Handler
{
    public interface IHandler
    {
        void Hand(Data.Products.Product[] products);
    }
}
