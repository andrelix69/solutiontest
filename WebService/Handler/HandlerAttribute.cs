﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Handler
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class HandlerAttribute:Attribute
    {
        private string _type;

        public string Type => _type;
        public HandlerAttribute(string type)
        {
            _type = type;
        }
    }
}
