﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebService.Data.Orders;
using WebService.Data.Products;
using WebService.Data.SystemTypes;

namespace WebService.HostedServices
{
    public class WorkerService : IHostedService // Требование соблюдено один класс 
    {
        Task _actionTask=null;
        private readonly CancellationTokenSource _stopping = new CancellationTokenSource();
        IOrderRepository _orderRepository;
        ILogger<WorkerService> _logger;
        SystemTypesEntity[] _systemTypes;
        public WorkerService(IOrderRepository orderRepository, ILogger<WorkerService> logger, ISystemTypesRepository systemTypesRepository)
        {
            _orderRepository = orderRepository;
            _logger = logger;
            _systemTypes = systemTypesRepository.GetSystemTypes();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _actionTask = StarAsync(_stopping.Token);
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_actionTask == null)
            {
                return;
            }
            try
            {
                _stopping.Cancel();
            }
            finally
            {
                await Task.WhenAny(_actionTask, Task.Delay(Timeout.Infinite, cancellationToken));
            }
        }

        public Task StarAsync(CancellationToken cancellationToken)
        {
            return Task.Run(async() => 
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);
                        var odrers = _orderRepository.GetNewOrderEntities();
                        foreach(var order in odrers)
                        {
                            try
                            {
                                string type = _systemTypes.SingleOrDefault(s => s.Id == order.SystemType)?.Name;
                                var handler = Handler.HandlerFactory.GetHandler(type);
                                OrderRequest orderRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderRequest>(order.SourceOrder);
                                handler.Hand(orderRequest.Products);
                                order.ConvertedOrder = Newtonsoft.Json.JsonConvert.SerializeObject(orderRequest);
                                await _orderRepository.UpdateAsync(order);
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex, ex.Message); //логирование сделал более стандартно
                            }
                        }

                    }
                    catch(Exception ex)
                    {
                        _logger.LogError(ex,ex.Message); //логирование сделал более стандартно
                    }
                }
            });
        }
    }
}
